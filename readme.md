## Introduction

This is an example package for the Interactive Broker Api Client **version 9.71** (IB Api Client) that is given at http://interactivebrokers.github.io . This package is a re-compilation of the IB Api Client, with all the necessary jars and files in the correct location.

I created this example package because I find the original IB Api Client is not well organised. It took me some time to understand what is happening in the original IB Api package. So I have re-structured the code, attached necessary jars, and moved the file in the correct place in this package.

The aim of this package is to provide a better package for new user of the IB Api client and save them some time in starting with it.

## Getting Started
To start the page, run the Main.java located under the package TestJavaClient.

Here is a screen shot of successful starting:
![ib_api_client.jpg](https://bitbucket.org/repo/zyyboy/images/2726403533-ib_api_client.jpg)